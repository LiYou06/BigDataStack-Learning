# Spark大数据处理框架入门-包括生态系统、运行流程以及部署方式



# **Spark 大数据处理框架简介**

![Spark大数据处理框架入门-包括生态系统、运行流程以及部署方式](http://p1.pstatp.com/large/pgc-image/77bfe859185d41c5b8fe38bfcab47c36)



Apache Spark 是专为大规模数据处理而设计的快速通用的计算引擎。Spark是UC Berkeley AMP lab (加州大学伯克利分校的AMP实验室)所开源的类Hadoop MapReduce的通用并行框架。Spark，拥有Hadoop MapReduce所具有的优点；但不同于MapReduce的是——**Job中间输出结果可以保存在内存中，从而不再需要读写HDFS，因此Spark能更好地适用于数据挖掘与机器学习等需要迭代的MapReduce的算法**。

[Spark 框架核心组件，以及Spark和Hadoop生态关系](https://www.toutiao.com/i6688868471540810253/?group_id=6688868471540810253)

**Spark具有如下4个主要特点：**

- 运行速度快 - Spark 使用先进的DAG（Directed Acyclic Graph，有向无环图）执行引擎，以支持循环数据流与内存计算，基于内存的执行速度可比Hapdoop MapReduce快上百倍。
- 容易使用 - Spark 支持使用Scale、Java、Python等，并且可以通过Spark Shell进行交互式编程。
- 通用性 - Spark 提供了完整的技术栈，包括SQL查询、流式计算、机器学习等组件。
- 运行模式多样 - Spark 可以运行于独立的集群模式，或者运行于Hadoop中，也可以运行于Amazon EC2等云环境中，并且可以访问 HDFS、Cassandra、HBase、Hive等多种数据源。

# **Spark 生态系统**

Spark的设计遵循“一个软件栈满足不同应用场景”的理念，逐渐形成了一套完整的生态系统，**既能够提供内存计算框架，也可以支持SQL即席查询、实时流式计算、机器学习和图计算等。**

![Spark大数据处理框架入门-包括生态系统、运行流程以及部署方式](http://p1.pstatp.com/large/pgc-image/b8846e8f863c45a495e46cf9f435eb71)



Spark 专注于数据的处理分析，而数据的存储还是借助于Hadoop HDFS、Amazon S3等来实现。

Spark 可以部署在资源管理器YARN之上。

Spark 生态系统主要包含了：

- Spark Core
- Spark SQL
- Spark Streaming
- MLlib（机器学习）
- GraphX（图计算）

# **Spark 运行流程**

Spark 运行流程如下所示。

（1）当一个Spark 应用被提交时，首先需要为这个应用构建起基本的运行环境，即由任务控制节点（Driver）创建一个SparkContext，由SparkContext 负责和集群管理器（Cluster Manager）的通信以及资源的申请、任务的分配和监控等。

![Spark大数据处理框架入门-包括生态系统、运行流程以及部署方式](http://p3.pstatp.com/large/pgc-image/b0ad944e458c4c8cbde59fc2d933525c)



（2）集群管理器为Executor 分配资源，并启动Executor 进程，Executor 运行情况将随着心跳发送给Cluster Manager上。

（3）SparkContext 根据RDD 的依赖关系构建DAG 图，DAG图提交给DAG 调度器（DAG Scheduler）进行解析，将DAG 图分解成多个阶段（Stage，每个阶段都是一个任务集），并且计算出各个阶段之间的依赖关系，然后把一个个任务集提交给底层的任务调度器（Task Scheduler）进行处理。

![Spark大数据处理框架入门-包括生态系统、运行流程以及部署方式](http://p3.pstatp.com/large/pgc-image/0f0f98387be64ad6b11ad7e3b80b518b)



任务调度器将任务（Task）分发给Executor 执行，同时SparkContext 将应用程序代码发送给 Executor。

（4）任务在Executor 上运行，把执行结果反馈给任务调度器，然后反馈给DAG 调度器，运行完毕后写入数据并释放所有资源。

# **Spark 部署方式**

目前，Spark 支持3中不同类型的部署方式，包括Standalone、Spark on YARN和Spark on Mesos。

![Spark大数据处理框架入门-包括生态系统、运行流程以及部署方式](http://p1.pstatp.com/large/pgc-image/4135c4b4c4d644789c78b4df30f25868)



- Standalone- Spark独立部署意味着Spark占据HDFS（Hadoop分布式文件系统）顶部的位置，Spark 框架自身也自带了完整的资源调度管理服务，可以独立部署到一个集群中。
- Spark on YARN - Spark 可以运行于YARN之上，和Hadoop 进行统一部署。资源管理和调度依赖YARN，分布式存储依赖HDFS。
- Spark on Mesos - Mesos 是一种资源调度管理框架，可以为运行在它上面的Spark 提供资源调度服务。

在Spark出现之前，为了能同时进行批处理和流出来，通常会采用 Hadoop+Storm架构。

在这种部署架构中，Hadoop和Storm框架部署在资源管理框架YARN/Mesos之上，接受统一的资源管理和调度，并共享底层的数据存储（HDFS、HBase、Cassandra等）。

Hadoop 负责对批量历史数据的实时查询和离线分析，而Storm 则负责对流数据的实时处理。如下图所示。

![Spark大数据处理框架入门-包括生态系统、运行流程以及部署方式](http://p1.pstatp.com/large/pgc-image/ecfa61d07bda49bb851b806fa9240d11)



上面的架构部署布局繁琐。由于Spark同时支持批处理和流程，因此可以从上述的Hadoop+Storm 架构，转向Spark 架构。

![Spark大数据处理框架入门-包括生态系统、运行流程以及部署方式](http://p9.pstatp.com/large/pgc-image/02fe6c8cba0c4dbd8a43121e958e981e)



其中Sparking Streaming的原理是将流数据分解成一系列短小的批处理作业，每个短小的批处理作业使用面向批处理的Spark Core进行处理，通过这种方式变相实现流计算，而不是真正实时的流计算，因而通常无法实现毫秒级的响应。