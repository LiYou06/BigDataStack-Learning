# Spark 框架核心组件，以及Spark和Hadoop生态关系

Spark框架是一个快速且API丰富的内存计算框架。Spark 采用Scala语言编写。

![Spark 框架核心组件，以及Spark和Hadoop生态关系](http://p9.pstatp.com/large/pgc-image/c9cb00feff5e458f95093fc41c0eef32)



相对于第一代的大数据生态系统Hadoop中的MapReduce，Spark 无论是在性能还是在方案的统一性方面，都有着极大的优势。Spark框架包含了多个紧密集成的组件，如下图所示。

# **1. Spark Core**

位于底层的是Spark Core，其实现了Spark的作业调度、内存管理、容错、与存储系统交互等基本功能，并针对弹性分布式数据集（RDD，Resilient Distributed Dataset）提供了丰富的操作。在Spark Core的基础上，Spark提供了一系列面向不同应用需求的组件，主要有Spark SQL、Spark Streaming、MLlib、GraphX。

![Spark 框架核心组件，以及Spark和Hadoop生态关系](http://p1.pstatp.com/large/pgc-image/0ca71bf390f64fb8af0487652ad138cc)



# **2. Spark SQL**

Spark SQL是Spark用来操作结构化数据的组件。通过Spark SQL，用户可以使用SQL或者Apache Hive版本的SQL方言（HQL）来查询数据。Spark SQL支持多种数据源类型，例如Hive表、Parquet以及JSON等。Spark SQL不仅为Spark提供了一个SQL接口，还支持开发者将SQL语句融入到Spark应用程序开发过程中，无论是使用Python、Java还是Scala，用户可以在单个的应用中同时进行SQL查询和复杂的数据分析。由于能够与Spark所提供的丰富的计算环境紧密结合，Spark SQL得以从其他开源数据仓库工具中脱颖而出。Spark SQL在Spark l.0中被首次引入。在Spark SQL之前，美国加州大学伯克利分校曾经尝试修改Apache Hive以使其运行在Spark上，进而提出了组件Shark。然而随着Spark SQL的提出与发展，其与Spark引擎和API结合得更加紧密，使得Shark已经被Spark SQL所取代。

# **3. Spark Streaming**

众多应用领域对实时数据的流式计算有着强烈的需求，例如网络环境中的网页服务器日志或是由用户提交的状态更新组成的消息队列等，这些都是实时数据流。Spark Streaming是Spark平台上针对实时数据进行流式计算的组件，提供了丰富的处理数据流的API。由于这些API与Spark Core中的基本操作相对应，因此开发者在熟知Spark核心概念与编程方法之后，编写Spark Streaming应用程序会更加得心应手。从底层设计来看，Spark Streaming支持与Spark Core同级别的容错性、吞吐量以及可伸缩性。

在Spark Streaming中，流处理实际用的是Micro-Batch的方式，即微批处理。

# **4. MLlib**

MLlib是Spark提供的一个机器学习算法库，其中包含了多种经典、常见的机器学习算法，主要有分类、回归、聚类、协同过滤等。MLlib不仅提供了模型评估、数据导入等额外的功能，还提供了一些更底层的机器学习原语，包括一个通用的梯度下降优化基础算法。所有这些方法都被设计为可以在集群上轻松伸缩的架构。

# **5. GraphX**

GraphX是Spark面向图计算提供的框架与算法库。GraphX中提出了弹性分布式属性图的概念，并在此基础上实现了图视图与表视图的有机结合与统一；同时针对图数据处理提供了丰富的操作，例如取子图操作subgraph、顶点属性操作mapVertices、边属性操作mapEdges等。GraphX还实现了与Pregel的结合，可以直接使用一些常用图算法，如PageRank、三角形计数等。

# **Spark和Hadoop生态关系**

Hadoop和Spark是两种不同的大数据处理框架，下面将两者整理在一幅图中，展示全貌。

![Spark 框架核心组件，以及Spark和Hadoop生态关系](http://p1.pstatp.com/large/pgc-image/c32b95cbeaa9427db25b6d223d988d6b)



其中蓝色部分，是Hadoop生态系统组件，黄色部分是Spark生态组件，虽然他们是两种不同的大数据处理框架，但它们不是互斥的，Spark与Hadoop 中的MapReduce是一种相互共生的关系。Hadoop提供了Spark中许多没有的功能，比如分布式文件系统，而Spark 提供了实时内存计算，速度非常快。

有一点大家要注意，Spark并不是一定要依附于Hadoop才能生存，除了Hadoop的HDFS，还可以基于其他的云平台，只是大家一致认为Spark与Hadoop配合默契最好。

# **Spark 部署和运行**

Spark 可以运行在Hadoop、Apache Mesos、Kubernetes之上，也可以单独部署，或者运行在云上。它可以访问不同的数据源，如Cassandra和HBase等等。

![Spark 框架核心组件，以及Spark和Hadoop生态关系](http://p1.pstatp.com/large/pgc-image/3c611597a2b04426b02376702160f7cb)